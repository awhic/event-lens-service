# EasyFleet Service

[EasyFleet](https://event-lens-web.vercel.app/home)

[CodeShift Submission](https://devpost.com/software/easyfleet)

## Description

A Spring Boot microservice that serves as the Resource Server for the administration and monitoring of autonomous, all-electric vehicle fleets.

## Related

- [EasyFleet UI](https://gitlab.com/awhic/event-lens-web)
- [Flask Service](https://gitlab.com/awhic/devfile-sample-python-basic)
- [Mock OBD Service](https://gitlab.com/awhic/mock-obd-service)

## Demonstration Video
- https://www.youtube.com/watch?v=lCzTsCLxrzE

## Architecture Diagram
![arch_diagram_2.jpg](src/main/resources/assets/arch_diagram.jpg)

## Table of Contents

- [Local Installation](#local-installation)
    - [Quickstart](#quickstart)
- [Openshift Deployment](#openshift-deployment)
    - [Instructions](#instructions)
- [Usage](#usage)
- [API](#api)
- [Guides](#guides)
    - [Apache Kafka Local Installation Guide](#apache-kafka-local-installation-guide)
    - [Apache Kafka OpenShift Installation Guide](#apache-kafka-openshift-installation-guide)
- [Additional Resources](#additional-resources)
- [Contact](#contact)
- [License](#license)

## Local Installation

### Prerequisites

Before you begin, ensure you have the following installed:

- JDK 11 or later
- Maven 3.6 or later
- Apache Kafka 3.6.0 or later

EasyFleet Service is dependent on Zookeeper-less Apache Kafka ([Guide to Install](#apache-kafka-local-installation-guide)).

### Quickstart

Run the following command from your local Apache Kafka install to start the Kafka Server:
- `$ bin/kafka-server-start.sh config/kraft/server.properties`

Run the following commands from the project directory:

1. `mvn clean install`
2. `mvn spring-boot:run -Dspring-boot.run.profiles=local`

## OpenShift Deployment

### Prerequisites

EasyFleet Service is dependent on Zookeeper-less Apache Kafka ([Guide to Install](#apache-kafka-openshift-installation-guide)).
- The application will need a topic titled, `rtd-events`. You can create this via command-line or the Kafka UI created in the [Installation Guide](#apache-kafka-openshift-installation-guide).
- The messages are lightweight (typically 244 Bytes in size), so only minimal topic configurations are required (1 partition, 1 replication factor, 12 hours retention, etc.).

You will also need to create a ConfigMap, and add the following to "data":
`py-host: https://easy-fleet-flask-service-{NAMESPACE}.apps.sandbox-{SANDBOX_SUFFIX}.openshiftapps.com`
>Example: 
>>![img.png](img.png)

### Instructions

1. Navigate to "+Add" > "Import from Git":
    - Insert: https://gitlab.com/awhic/event-lens-service

![img.png](src/main/resources/assets/import-git.png)

2. Change the import strategy to "Dockerfile" and change the directory to `docker/Dockerfile`:

![img.png](src/main/resources/assets/import-strategy.png)

3. In the "General" section:
    - Application - Place in the same Application Group as the Main Application, or create a new one if not applicable.
    - Name - `easy-fleet-service`
    - Resource Type - **Serverless** Deployment
4. Set Target port to `8081`.
5. Check the box for "Create Route".
6. Open advanced options for "Build Configuration", select "Add from ConfigMap or Secret" and add the following from your ConfigMap:

![img.png](src/main/resources/assets/config-map-py-host.png)

7. Click "Create".

>Note: You can easily grab the value for py-host by hitting the default route once created by simply copying what is in the url bar and removing the trailing slash.

## Usage

EasyFleet Service is cloud-native and is designed to be deployed to OpenShift or other Kubernetes distributions. It can be and is best deployed serverless.
EasyFleet manages CRUD operations for vehicles. This includes reading and writing vehicle data, and handling administrative tasks such as trading driver assignments and updating vehicle license information.
When deployed serverless, EasyFleet spins down when there is no web traffic. This allows for cost savings when vehicles in your fleet are not in use.
EasyFleet consumes real-time vehicle data via Apache Kafka, and persists it. If EasyFleet is spun down, not to worry: it consumes on the latest offset, meaning it will always stay the most up-to-date when reading.

## API

>OpenAPI Documentation provided via SwaggerUI at: `/swagger-ui/index.html#/`.  If running locally, this is accessible [here](http://localhost:8080/swagger-ui/index.html#/).

Documentation and examples for all endpoints.

### Static Data

#### Get All Trucks

- **Endpoint**: `GET /api/v1/static/vehicles`
- **Description**: Fetch a list of all trucks in the fleet.

#### Get Truck by Driver

- **Endpoint**: `GET /api/v1/static/truck/driver?id={driverId}`
- **Description**: Retrieve truck details associated with a particular driver.
- **Parameters**:
    - `id`: The ID of the driver.

#### Update Truck Information

- **Endpoint**: `PUT /api/v1/static/truck/:id`
- **Description**: Update the information of a specific truck.
- **Headers**:
    - `Content-Type: application/json`
- **Body**:

```json
{
  "truck": {
    "truckId": 3,
    "truckModel": "Model T50",
    "yearOfManufacture": 2020,
    "truckManufacturer": "ElectroTrucks",
    "batteryCapacity": 700,
    "mileage": 17000,
    "truckVin": "3C4D5E6F7G8H1A2B",
    "licensePlate": "DEF456",
    "state": null
  },
  "driver": {
    "driverId": 3,
    "firstName": "William",
    "lastName": "Johnson"
  }
}
```

#### Trade Truck

- **Endpoint**: `PUT /api/v1/static/truck/trade`
- **Description**: Update the data related to the trading of trucks.
- **Headers**:
    - `Content-Type: application/json`
- **Body**:

```json
[
  {
    "truck": {
      "truckId": 3,
      "truckModel": "Model T50",
      "yearOfManufacture": 2020,
      "truckManufacturer": "ElectroTrucks",
      "batteryCapacity": 700,
      "mileage": 16000,
      "truckVin": "3C4D5E6F7G8H1A2B",
      "licensePlate": "DEF456",
      "state": "NY"
    },
    "driver": {
      "driverId": 4,
      "firstName": "William",
      "lastName": "Johnson"
    }
  },
  {
    "truck": {
      "truckId": 2,
      "truckModel": "Model T200",
      "yearOfManufacture": 2022,
      "truckManufacturer": "FreightMakers",
      "batteryCapacity": 600,
      "mileage": 12000,
      "truckVin": "2B3C4D5E6F7G8H1A",
      "licensePlate": "ABC789",
      "state": "TX"
    },
    "driver": {
      "driverId": 2,
      "firstName": "John",
      "lastName": "Doe"
    }
  }
]
```
### Realtime Data

#### Get Real-Time Data by Truck Id

- **Endpoint**: `GET /api/v1/real-time/vehicle?id={truckId}`
- **Description**: Retrieve the real-time data for a truck by its ID.
- **Parameters**:
    - `id`: The ID of the truck.

## Guides

### Apache Kafka Local Installation Guide
1. [Download](https://www.apache.org/dyn/closer.cgi?path=/kafka/3.6.0/kafka_2.13-3.6.0.tgz) Apache Kafka and extract it:
    - `tar -xzf kafka_2.13-3.6.0.tgz`
    - `cd kafka_2.13-3.6.0`
2. Start the Kafka Environment:
    - `KAFKA_CLUSTER_ID="$(bin/kafka-storage.sh random-uuid)"`
    - `bin/kafka-storage.sh format -t $KAFKA_CLUSTER_ID -c config/kraft/server.properties`
    - `bin/kafka-server-start.sh config/kraft/server.properties`
3. Create the topic:
    - `bin/kafka-topics.sh --create --topic rtd-events --bootstrap-server localhost:9092`
4. (Optionally) test that the server is running and messages can be produced and consumed:
    - `bin/kafka-console-producer.sh --topic rtd-events --bootstrap-server localhost:9092`
        - Note: every new line is a new message to produce.
    - `bin/kafka-console-consumer.sh --topic rtd-events --from-beginning --bootstrap-server localhost:9092`

### Apache Kafka OpenShift Installation Guide
- Follow along with this tutorial: https://www.youtube.com/watch?v=QxnWL45FgdY
- The YAML files used are provided here:
  - [Deployment and Service](src/main/resources/assets/kafka-deploy-config.yml)
  - [Kafka UI](src/main/resources/assets/kafka-ui-config.yml)
  - [Helm Release](src/main/resources/assets/helm-config.yml)

## Additional Resources

- [Spring Boot](https://spring.io/projects/spring-boot)
    - [Spring Initializr](https://start.spring.io/)
- [Dockerfile](https://docs.docker.com/engine/reference/builder/)
    - [Spring Boot Code Sample](https://github.com/devfile-samples/devfile-sample-java-springboot-basic)
- [Apache Maven](https://maven.apache.org/guides/index.html)
    - [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/maven-plugin/reference/html/)
    - [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/maven-plugin/reference/html/#build-image)
- [Apache Kafka](https://kafka.apache.org/intro)
    - [Quickstart Guide](https://kafka.apache.org/quickstart)
    - [Franz Kafka](https://en.wikipedia.org/wiki/Franz_Kafka)

## Contact

For any inquiries, please feel free to reach out to me at [awhixk@gmail.com](mailto:awhixk@gmail.com).

If you need to speak to me, I can be reached at 502-322-5742.

## License

This project is licensed under the MIT License - see [LICENSE.md](LICENSE.md).
