-- Table: public.truck_real_time_data

-- DROP TABLE IF EXISTS public.truck_real_time_data;

CREATE TABLE IF NOT EXISTS public.truck_real_time_data
(
    data_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    truck_id integer NOT NULL,
    battery_status integer,
    current_mileage integer,
    motor_health character varying(50) COLLATE pg_catalog."default",
    error_codes character varying(255) COLLATE pg_catalog."default",
    brake_pad_condition integer,
    brake_fluid_level integer,
    tire_pressure_low boolean,
    air_conditioning_functional boolean,
    software_up_to_date boolean,
    "timestamp" timestamp with time zone NOT NULL,
    group_uuid character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT truck_fk FOREIGN KEY (truck_id)
    REFERENCES public.trucks (truck_id) MATCH SIMPLE
                          ON UPDATE NO ACTION
                          ON DELETE NO ACTION
    )

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.truck_real_time_data
    OWNER to postgres;

-- ALTER TABLE IF EXISTS public.truck_real_time_data
--     OWNER to awhixk;
