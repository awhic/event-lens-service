-- Table: public.trucks

-- DROP TABLE IF EXISTS public.trucks;

CREATE TABLE IF NOT EXISTS public.trucks
(
    truck_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    truck_model text COLLATE pg_catalog."default" NOT NULL,
    year_of_manufacture integer NOT NULL,
    truck_manufacturer text COLLATE pg_catalog."default" NOT NULL,
    battery_capacity integer NOT NULL,
    mileage integer NOT NULL,
    truck_vin text COLLATE pg_catalog."default" NOT NULL,
    license_plate text COLLATE pg_catalog."default" NOT NULL,
    state text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT trucks_pk PRIMARY KEY (truck_id)
    )

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.trucks
    OWNER to postgres;

-- ALTER TABLE IF EXISTS public.trucks
--     OWNER to awhixk;
