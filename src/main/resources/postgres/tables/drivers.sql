-- Table: public.drivers

-- DROP TABLE IF EXISTS public.drivers;

CREATE TABLE IF NOT EXISTS public.drivers
(
    driver_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    assigned_truck_id integer NOT NULL,
    first_name text COLLATE pg_catalog."default" NOT NULL,
    last_name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT driver_pk PRIMARY KEY (driver_id),
    CONSTRAINT trucks FOREIGN KEY (assigned_truck_id)
    REFERENCES public.trucks (truck_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    )

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.drivers
    OWNER to postgres;

-- ALTER TABLE IF EXISTS public.drivers
--     OWNER to awhixk;
