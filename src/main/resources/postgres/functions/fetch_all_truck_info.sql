-- FUNCTION: public.fetch_all_truck_info()

-- DROP FUNCTION IF EXISTS public.fetch_all_truck_info();

CREATE OR REPLACE FUNCTION public.fetch_all_truck_info(
	OUT ref_cursor refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
OPEN ref_cursor FOR
SELECT
    t.truck_id,
    t.truck_model,
    t.year_of_manufacture,
    t.truck_manufacturer,
    t.battery_capacity,
    t.mileage,
    t.truck_vin,
    t.license_plate,
    t.state,
    d.driver_id,
    d.first_name,
    d.last_name
FROM
    public.trucks t
        LEFT JOIN
    public.drivers d ON t.truck_id = d.assigned_truck_id;
END;
$BODY$;

ALTER FUNCTION public.fetch_all_truck_info()
    OWNER TO postgres;

-- ALTER FUNCTION public.fetch_all_truck_info()
--     OWNER TO awhixk;
