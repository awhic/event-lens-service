-- FUNCTION: public.fetch_real_time_data_by_truck_id(integer)

-- DROP FUNCTION IF EXISTS public.fetch_real_time_data_by_truck_id(integer);

CREATE OR REPLACE FUNCTION public.fetch_real_time_data_by_truck_id(
	pi_truck_id integer,
	OUT ref_cursor refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
OPEN ref_cursor FOR
SELECT
    r.data_id,
    r.truck_id,
    r.battery_status,
    r.current_mileage,
    r.motor_health,
    r.error_codes,
    r.brake_pad_condition,
    r.brake_fluid_level,
    r.tire_pressure_low,
    r.air_conditioning_functional,
    r.software_up_to_date,
    r.timestamp,
    r.group_uuid
FROM
    public.truck_real_time_data r
WHERE
        r.truck_id = pi_truck_id
ORDER BY
    r.timestamp DESC
    LIMIT 1; -- Get only the most recent row
END;
$BODY$;

ALTER FUNCTION public.fetch_real_time_data_by_truck_id(integer)
    OWNER TO postgres;

-- ALTER FUNCTION public.fetch_real_time_data_by_truck_id(integer)
--     OWNER TO awhixk;
