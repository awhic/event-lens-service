-- FUNCTION: public.trade_truck_assignment(integer, integer)

-- DROP FUNCTION IF EXISTS public.trade_truck_assignment(integer, integer);

CREATE OR REPLACE FUNCTION public.trade_truck_assignment(
	pi_driver_one_id integer,
	pi_driver_two_id integer,
	OUT ref_cursor refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
v_driver_one_truck_id integer;
    v_driver_two_truck_id integer;
BEGIN
SELECT assigned_truck_id INTO v_driver_one_truck_id FROM drivers WHERE driver_id = pi_driver_one_id;
SELECT assigned_truck_id INTO v_driver_two_truck_id FROM drivers WHERE driver_id = pi_driver_two_id;

UPDATE drivers SET assigned_truck_id = v_driver_two_truck_id WHERE driver_id = pi_driver_one_id;
UPDATE drivers SET assigned_truck_id = v_driver_one_truck_id WHERE driver_id = pi_driver_two_id;

OPEN ref_cursor FOR
SELECT
    t.truck_id,
    t.truck_model,
    t.year_of_manufacture,
    t.truck_manufacturer,
    t.battery_capacity,
    t.mileage,
    t.truck_vin,
    t.license_plate,
    t.state,
    d.driver_id,
    d.first_name,
    d.last_name
FROM
    trucks t
        INNER JOIN
    drivers d ON t.truck_id = d.assigned_truck_id
WHERE
        d.driver_id IN (pi_driver_one_id, pi_driver_two_id);

END;
$BODY$;

ALTER FUNCTION public.trade_truck_assignment(integer, integer)
    OWNER TO postgres;

-- ALTER FUNCTION public.trade_truck_assignment(integer, integer)
--     OWNER TO awhixk;
