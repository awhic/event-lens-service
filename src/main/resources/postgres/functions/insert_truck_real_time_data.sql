-- FUNCTION: public.insert_truck_real_time_data(integer, integer, integer, character varying, character varying, integer, integer, boolean, boolean, boolean, timestamp with time zone, character varying)

-- DROP FUNCTION IF EXISTS public.insert_truck_real_time_data(integer, integer, integer, character varying, character varying, integer, integer, boolean, boolean, boolean, timestamp with time zone, character varying);

CREATE OR REPLACE FUNCTION public.insert_truck_real_time_data(
	pi_truck_id integer,
	pi_battery_status integer,
	pi_current_mileage integer,
	pi_motor_health character varying,
	pi_error_codes character varying,
	pi_brake_pad_condition integer,
	pi_brake_fluid_level integer,
	pi_tire_pressure_low boolean,
	pi_air_conditioning_functional boolean,
	pi_software_up_to_date boolean,
	pi_timestamp timestamp with time zone,
	pi_group_uuid character varying,
	OUT rows_inserted integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
INSERT INTO public.truck_real_time_data(
    truck_id,
    battery_status,
    current_mileage,
    motor_health,
    error_codes,
    brake_pad_condition,
    brake_fluid_level,
    tire_pressure_low,
    air_conditioning_functional,
    software_up_to_date,
    timestamp,
    group_uuid
)
VALUES (
           pi_truck_id,
           pi_battery_status,
           pi_current_mileage,
           pi_motor_health,
           pi_error_codes,
           pi_brake_pad_condition,
           pi_brake_fluid_level,
           pi_tire_pressure_low,
           pi_air_conditioning_functional,
           pi_software_up_to_date,
           pi_timestamp,
           pi_group_uuid
       );

GET DIAGNOSTICS rows_inserted = ROW_COUNT;

END;
$BODY$;

ALTER FUNCTION public.insert_truck_real_time_data(integer, integer, integer, character varying, character varying, integer, integer, boolean, boolean, boolean, timestamp with time zone, character varying)
    OWNER TO postgres;

-- ALTER FUNCTION public.insert_truck_real_time_data(integer, integer, integer, character varying, character varying, integer, integer, boolean, boolean, boolean, timestamp with time zone, character varying)
--     OWNER TO awhixk;
