-- FUNCTION: public.update_truck_by_truck_id(integer, character varying DEFAULT NULL, integer DEFAULT NULL, character varying DEFAULT NULL, integer DEFAULT NULL, integer DEFAULT NULL, character varying DEFAULT NULL, character varying DEFAULT NULL, character varying DEFAULT NULL)

-- DROP FUNCTION IF EXISTS public.update_truck_by_truck_id(integer, character varying, integer, character varying, integer, integer, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.update_truck_by_truck_id(
    pi_truck_id integer,
    pi_truck_model character varying DEFAULT NULL,
    pi_year_of_manufacture integer DEFAULT NULL,
    pi_truck_manufacturer character varying DEFAULT NULL,
    pi_battery_capacity integer DEFAULT NULL,
    pi_mileage integer DEFAULT NULL,
    pi_truck_vin character varying DEFAULT NULL,
    pi_license_plate character varying DEFAULT NULL,
    pi_state character varying DEFAULT NULL,
    OUT ref_cursor refcursor)
RETURNS refcursor
LANGUAGE 'plpgsql'
COST 100
VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
UPDATE trucks
SET truck_model = COALESCE(pi_truck_model, truck_model),
    year_of_manufacture = COALESCE(pi_year_of_manufacture, year_of_manufacture),
    truck_manufacturer = COALESCE(pi_truck_manufacturer, truck_manufacturer),
    battery_capacity = COALESCE(pi_battery_capacity, battery_capacity),
    mileage = COALESCE(pi_mileage, mileage),
    truck_vin = COALESCE(pi_truck_vin, truck_vin),
    license_plate = COALESCE(pi_license_plate, license_plate),
    state = COALESCE(pi_state, state)
WHERE truck_id = pi_truck_id;

OPEN ref_cursor FOR
SELECT
    t.truck_id,
    t.truck_model,
    t.year_of_manufacture,
    t.truck_manufacturer,
    t.battery_capacity,
    t.mileage,
    t.truck_vin,
    t.license_plate,
    t.state,
    d.driver_id,
    d.first_name,
    d.last_name
FROM
    trucks t
        LEFT JOIN
    drivers d ON t.truck_id = d.assigned_truck_id
WHERE
        t.truck_id = pi_truck_id;

END;
$BODY$;

ALTER FUNCTION public.update_truck_by_truck_id(integer, character varying, integer, character varying, integer, integer, character varying, character varying, character varying)
    OWNER TO postgres;

-- ALTER FUNCTION public.update_truck_by_truck_id(integer, character varying, integer, character varying, integer, integer, character varying, character varying, character varying)
--     OWNER TO awhixk;
