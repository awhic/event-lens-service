package com.openshiftapps.eventlens.staticdata.dao;

import com.openshiftapps.eventlens.staticdata.model.Vehicle;
import com.openshiftapps.eventlens.staticdata.model.VehicleInfo;

import java.util.List;

public interface StaticDataDao {
    List<VehicleInfo> fetchVehicles();

    VehicleInfo fetchVehicleByDriver(Integer id);

    VehicleInfo updateVehicle(Vehicle vehicle, Integer vehicleId);

    List<VehicleInfo> tradeVehicleAssignment(Integer driverOne, Integer driverTwo);
}
