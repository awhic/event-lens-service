package com.openshiftapps.eventlens.staticdata.dao;

import com.openshiftapps.eventlens.staticdata.model.Vehicle;
import com.openshiftapps.eventlens.staticdata.model.VehicleInfo;
import com.openshiftapps.eventlens.staticdata.model.Driver;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class VehicleInfoRowMapper implements RowMapper<VehicleInfo> {
    @Override
    public VehicleInfo mapRow(ResultSet rs, int rowNum) throws SQLException {

        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(rs.getInt("truck_id"));
        vehicle.setModel(rs.getString("truck_model"));
        vehicle.setYearOfManufacture(rs.getInt("year_of_manufacture"));
        vehicle.setManufacturer(rs.getString("truck_manufacturer"));
        vehicle.setBatteryCapacity(rs.getInt("battery_capacity"));
        vehicle.setMileage(rs.getInt("mileage"));
        vehicle.setVin(rs.getString("truck_vin"));
        vehicle.setLicensePlate(rs.getString("license_plate"));
        vehicle.setState(rs.getString("state"));

        Driver driver = new Driver();
        driver.setDriverId(rs.getInt("driver_id"));
        driver.setFirstName(rs.getString("first_name"));
        driver.setLastName(rs.getString("last_name"));

        VehicleInfo vehicleInfo = new VehicleInfo();
        vehicleInfo.setVehicle(vehicle);
        vehicleInfo.setDriver(driver);

        return vehicleInfo;
    }
}

