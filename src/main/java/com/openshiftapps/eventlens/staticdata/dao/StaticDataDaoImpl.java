package com.openshiftapps.eventlens.staticdata.dao;

import com.openshiftapps.eventlens.staticdata.model.Vehicle;
import com.openshiftapps.eventlens.staticdata.model.VehicleInfo;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;
import java.util.Map;


/**
 * Implementation of the {@link StaticDataDao} interface providing data access operations for vehicle data.
 *
 * <p>Methods include:
 * <ul>
 *   <li><b>fetchVehicles:</b> Retrieves a list of all vehicles, encapsulated in {@link VehicleInfo} objects.</li>
 *   <li><b>fetchVehicleByDriver:</b> Fetches vehicle information for a specific driver, identified by the driver's ID.</li>
 *   <li><b>updateVehicle:</b> Updates the information of a vehicle, identified by a vehicle ID, and returns the updated {@link VehicleInfo}.</li>
 *   <li><b>tradeVehicleAssignment:</b> Facilitates the trading of vehicle assignments between two drivers, identified by their respective driver IDs, and returns the list of updated {@link VehicleInfo}.</li>
 * </ul>
 *
 * <p>The JDBC calls are initialized upon bean construction for efficiency.</p>
 *
 * <p><b>Note:</b> Data Layer was designed in an early conception where the application worked for long-haul trucks,
 * hence the mentions of "truck". The application is designed for all types of electric vehicle fleets.<p>
 *
 * @see VehicleInfo
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class StaticDataDaoImpl implements StaticDataDao {

    private final JdbcTemplate staticDataJdbcTemplate;

    private SimpleJdbcCall fetchAllVehicleInfoCall;
    private SimpleJdbcCall fetchVehicleInfoByDriverCall;
    private SimpleJdbcCall updateVehicleByIdCall;
    private SimpleJdbcCall tradeVehicleAssignmentCall;

    @PostConstruct
    void initialize() {
        fetchAllVehicleInfoCall = new SimpleJdbcCall(staticDataJdbcTemplate)
                .withFunctionName("fetch_all_truck_info")
                .declareParameters(
                        new SqlOutParameter("ref_cursor", Types.REF_CURSOR, new VehicleInfoRowMapper())
                )
                .withoutProcedureColumnMetaDataAccess();
        log.info("Initialized {}", fetchAllVehicleInfoCall.getProcedureName());

        fetchVehicleInfoByDriverCall = new SimpleJdbcCall(staticDataJdbcTemplate)
                .withFunctionName("fetch_truck_by_driver_id")
                .declareParameters(
                        new SqlParameter("pi_driver_id", Types.INTEGER),
                        new SqlOutParameter("ref_cursor", Types.REF_CURSOR, new VehicleInfoRowMapper())
                )
                .withoutProcedureColumnMetaDataAccess();
        log.info("Initialized {}", fetchVehicleInfoByDriverCall.getProcedureName());

        updateVehicleByIdCall = new SimpleJdbcCall(staticDataJdbcTemplate)
                .withFunctionName("update_truck_by_truck_id")
                .declareParameters(
                        new SqlParameter("pi_truck_id", Types.INTEGER),
                        new SqlParameter("pi_truck_model", Types.VARCHAR),
                        new SqlParameter("pi_year_of_manufacture", Types.INTEGER),
                        new SqlParameter("pi_truck_manufacturer", Types.VARCHAR),
                        new SqlParameter("pi_battery_capacity", Types.INTEGER),
                        new SqlParameter("pi_mileage", Types.INTEGER),
                        new SqlParameter("pi_truck_vin", Types.VARCHAR),
                        new SqlParameter("pi_license_plate", Types.VARCHAR),
                        new SqlParameter("pi_state", Types.VARCHAR),
                        new SqlOutParameter("ref_cursor", Types.REF_CURSOR, new VehicleInfoRowMapper())
                )
                .withoutProcedureColumnMetaDataAccess();
        log.info("Initialized {}", updateVehicleByIdCall.getProcedureName());

        tradeVehicleAssignmentCall = new SimpleJdbcCall(staticDataJdbcTemplate)
                .withFunctionName("trade_truck_assignment")
                .declareParameters(
                        new SqlParameter("pi_driver_one_id", Types.INTEGER),
                        new SqlParameter("pi_driver_two_id", Types.INTEGER),
                        new SqlOutParameter("ref_cursor", Types.REF_CURSOR, new VehicleInfoRowMapper())
                )
                .withoutProcedureColumnMetaDataAccess();
        log.info("Initialized {}", tradeVehicleAssignmentCall.getProcedureName());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VehicleInfo> fetchVehicles() {
        SqlParameterSource in = new MapSqlParameterSource();
        Map<String, Object> out = fetchAllVehicleInfoCall.execute(in);

        log.trace("fetchTrucks(): RefCursor: {}", out.get("ref_cursor"));
        return (List<VehicleInfo>) out.get("ref_cursor");
    }

    @Override
    @SuppressWarnings("unchecked")
    public VehicleInfo fetchVehicleByDriver(Integer id) {
        SqlParameterSource in = new MapSqlParameterSource().addValue("pi_driver_id", id);
        Map<String, Object> out = fetchVehicleInfoByDriverCall.execute(in);

        log.debug("fetchTruckByDriver() parameters: {}", in);
        log.trace("RefCursor: {}", out.get("ref_cursor"));
        return ((List<VehicleInfo>) out.get("ref_cursor")).get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public VehicleInfo updateVehicle(Vehicle vehicle, Integer vehicleId) {
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pi_truck_id", vehicleId)
                .addValue("pi_truck_model", vehicle.getModel())
                .addValue("pi_year_of_manufacture", vehicle.getYearOfManufacture())
                .addValue("pi_truck_manufacturer", vehicle.getManufacturer())
                .addValue("pi_battery_capacity", vehicle.getBatteryCapacity())
                .addValue("pi_mileage", vehicle.getMileage())
                .addValue("pi_truck_vin", vehicle.getVin())
                .addValue("pi_license_plate", vehicle.getLicensePlate())
                .addValue("pi_state", vehicle.getState());
        Map<String, Object> out = updateVehicleByIdCall.execute(in);

        log.debug("updateTruck(): parameters: {}", in);
        log.trace("RefCursor: {}", out.get("ref_cursor"));
        return ((List<VehicleInfo>) out.get("ref_cursor")).get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VehicleInfo> tradeVehicleAssignment(Integer driverOne, Integer driverTwo) {
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pi_driver_one_id", driverOne)
                .addValue("pi_driver_two_id", driverTwo);
        Map<String, Object> out = tradeVehicleAssignmentCall.execute(in);

        log.debug("tradeTruckAssignment(): parameters: {}", in);
        log.trace("RefCursor: {}", out.get("ref_cursor"));
        return (List<VehicleInfo>) out.get("ref_cursor");
    }
}
