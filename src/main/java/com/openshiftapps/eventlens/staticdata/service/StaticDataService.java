package com.openshiftapps.eventlens.staticdata.service;

import com.openshiftapps.eventlens.exception.ItemNotFoundException;
import com.openshiftapps.eventlens.staticdata.model.Vehicle;
import com.openshiftapps.eventlens.staticdata.model.VehicleInfo;

import java.util.List;

public interface StaticDataService {
    List<VehicleInfo> fetchVehicles();

    VehicleInfo fetchVehicleByDriver(Integer id) throws ItemNotFoundException;

    VehicleInfo updateVehicle(Vehicle vehicle, Integer id);

    List<VehicleInfo> tradeVehicleAssignment(Integer driverOne, Integer driverTwo);
}
