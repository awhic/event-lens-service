package com.openshiftapps.eventlens.staticdata.service;

import com.openshiftapps.eventlens.exception.ItemNotFoundException;
import com.openshiftapps.eventlens.staticdata.dao.StaticDataDao;
import com.openshiftapps.eventlens.staticdata.model.Vehicle;
import com.openshiftapps.eventlens.staticdata.model.VehicleInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StaticDataServiceImpl implements StaticDataService {

    private final StaticDataDao staticDataDao;
    @Override
    @Transactional
    public List<VehicleInfo> fetchVehicles() {
        return staticDataDao.fetchVehicles();
    }

    @Override
    @Transactional
    public VehicleInfo fetchVehicleByDriver(Integer id) throws ItemNotFoundException {
        try {
            return staticDataDao.fetchVehicleByDriver(id);
        } catch (Exception e) {
            throw new ItemNotFoundException();
        }
    }

    @Override
    @Transactional
    public VehicleInfo updateVehicle(Vehicle vehicle, Integer id) {
        return staticDataDao.updateVehicle(vehicle, id);
    }

    @Override
    @Transactional
    public List<VehicleInfo> tradeVehicleAssignment(Integer driverOne, Integer driverTwo) {
        return staticDataDao.tradeVehicleAssignment(driverOne, driverTwo);
    }
}
