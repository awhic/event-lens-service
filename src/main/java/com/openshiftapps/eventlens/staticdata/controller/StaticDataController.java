package com.openshiftapps.eventlens.staticdata.controller;

import com.openshiftapps.eventlens.exception.ItemNotFoundException;
import com.openshiftapps.eventlens.staticdata.model.VehicleInfo;
import com.openshiftapps.eventlens.staticdata.service.StaticDataService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping("api/v1/static")
public class StaticDataController {

    private final StaticDataService staticDataService;

    @GetMapping("/vehicles")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Fetch the information for all vehicles")
    public List<VehicleInfo> fetchAllVehicles() {
        return staticDataService.fetchVehicles();
    }

    @GetMapping("/vehicle/driver")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Fetch the information for one vehicle by the driver")
    public VehicleInfo fetchVehicleByDriver(@RequestParam Integer id) throws ItemNotFoundException {
        return staticDataService.fetchVehicleByDriver(id);
    }

    @PutMapping("vehicle/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update the information for one vehicle by the driver")
    public VehicleInfo updateVehicleByDriverId(@PathVariable("id") Integer vehicleId, @RequestBody VehicleInfo vehicleInfo) {
        return staticDataService.updateVehicle(vehicleInfo.getVehicle(), vehicleId);
    }

    @PutMapping("vehicle/trade")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Trade vehicle assignments between drivers by providing the two vehicles you would like to trade")
    public LinkedList<VehicleInfo> tradeVehicleAssignment(@RequestBody LinkedList<VehicleInfo> vehicleInfoList) {
        List<VehicleInfo> updatedVehicleInfoList = staticDataService.tradeVehicleAssignment(
                vehicleInfoList.get(0).getDriver().getDriverId(),
                vehicleInfoList.get(1).getDriver().getDriverId()
        );
        vehicleInfoList.addAll(updatedVehicleInfoList);
        return vehicleInfoList;
    }
}
