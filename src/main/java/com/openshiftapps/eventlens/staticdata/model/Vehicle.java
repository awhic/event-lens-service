package com.openshiftapps.eventlens.staticdata.model;

import lombok.Data;

@Data
public class Vehicle {
    private Integer vehicleId;
    private String model;
    private Integer yearOfManufacture;
    private String manufacturer;
    private Integer batteryCapacity;
    private Integer mileage;
    private String vin;
    private String licensePlate;
    private String state;
}

