package com.openshiftapps.eventlens.staticdata.model;

import lombok.Data;

@Data
public class Driver {
    private Integer driverId;
    private String firstName;
    private String lastName;
}
