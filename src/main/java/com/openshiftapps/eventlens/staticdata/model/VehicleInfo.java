package com.openshiftapps.eventlens.staticdata.model;

import lombok.Data;

@Data
public class VehicleInfo {
    Vehicle vehicle;
    Driver driver;
}
