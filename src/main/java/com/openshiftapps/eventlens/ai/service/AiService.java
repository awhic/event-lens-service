package com.openshiftapps.eventlens.ai.service;

import com.openshiftapps.eventlens.exception.NotEnoughDataException;

import java.time.Instant;

public interface AiService {
    Instant flaskAiInsights(Integer id, String uuid) throws NotEnoughDataException;
}
