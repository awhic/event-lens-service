package com.openshiftapps.eventlens.ai.service;

import com.openshiftapps.eventlens.ai.model.FlaskData;
import com.openshiftapps.eventlens.exception.NotEnoughDataException;
import com.openshiftapps.eventlens.realtimedata.dao.RealTimeDataDao;
import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AiServiceImpl implements AiService {

    @Value("${spring.host}")
    private String baseUrl;

    private final RestTemplate restTemplate;

    private final RealTimeDataDao realTimeDataDao;

    @Override
    @Transactional
    public Instant flaskAiInsights(Integer id, String uuid) throws NotEnoughDataException {
        String url = baseUrl + "/api/v1/insights";

        List<RealTimeData> realTimeDataList = realTimeDataDao.fetchRealTimeDataByGroupUuid(id, uuid);

        if (realTimeDataList.size() < 50) {
            throw new NotEnoughDataException();
        }

        List<FlaskData> flaskDataSet = new ArrayList<>();
        List<FlaskData> finalFlaskDataSet = flaskDataSet;
        realTimeDataList.forEach(realTimeData ->
                finalFlaskDataSet.add(FlaskData.builder()
                        .value(realTimeData.getBatteryStatus())
                        .time(realTimeData.getTimestamp().toInstant().toEpochMilli())
                        .build())
        );
        flaskDataSet = flaskDataSet.stream()
                .filter(flaskData -> !flaskData.getValue().equals(0))
                .sorted(Comparator.comparingLong(FlaskData::getTime)).collect(Collectors.toList());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<List<FlaskData>> requestEntity = new HttpEntity<>(flaskDataSet, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<>() {
                }
        );

        if (response.getBody() == null || response.getStatusCode() != HttpStatus.OK || response.getBody().equals("0")) {
            throw new HttpClientErrorException(response.getStatusCode());
        } else {
            return Instant.ofEpochMilli(Long.parseLong(response.getBody().substring(0, response.getBody().indexOf("."))));
        }
    }
}
