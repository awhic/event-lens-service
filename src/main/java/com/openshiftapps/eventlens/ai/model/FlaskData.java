package com.openshiftapps.eventlens.ai.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FlaskData {
    private Integer value;
    private Long time;
}
