package com.openshiftapps.eventlens;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@SpringBootApplication
public class EventLensApplication {

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Test the REST API with a simple `Hello World` output")
    String home() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        SpringApplication.run(EventLensApplication.class, args);
    }
}
