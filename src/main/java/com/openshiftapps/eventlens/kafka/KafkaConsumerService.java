package com.openshiftapps.eventlens.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;
import com.openshiftapps.eventlens.realtimedata.model.RealTimeDataWithId;
import com.openshiftapps.eventlens.realtimedata.service.RealTimeDataService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaConsumerService {

    private final RealTimeDataService realTimeDataService;

    @SneakyThrows
    @KafkaListener(topics = "rtd-events", groupId = "myGroup")
    public void consume(String message) {

        ObjectMapper objectMapper = new ObjectMapper();

        RealTimeDataWithId realTimeDataWithId = objectMapper.readValue(message, RealTimeDataWithId.class);
        log.trace("Received: {}", realTimeDataWithId);

        RealTimeData realTimeData = RealTimeData.builder()
                .batteryStatus(realTimeDataWithId.getBatteryStatus())
                .currentMileage(realTimeDataWithId.getCurrentMileage())
                .motorHealth(realTimeDataWithId.getMotorHealth())
                .errorCodes(realTimeDataWithId.getErrorCodes())
                .brakePadCondition(realTimeDataWithId.getBrakePadCondition())
                .brakeFluidLevel(realTimeDataWithId.getBrakeFluidLevel())
                .tirePressureLow(realTimeDataWithId.getTirePressureLow())
                .airConditioningFunctional(realTimeDataWithId.getAirConditioningFunctional())
                .softwareUpToDate(realTimeDataWithId.getSoftwareUpToDate())
                .timestamp(realTimeDataWithId.getTimestamp())
                .groupUuid(realTimeDataWithId.getGroupUuid())
                .build();

        realTimeDataService.insertRealTimeData(realTimeDataWithId.getVehicleId(), realTimeData);
    }
}
