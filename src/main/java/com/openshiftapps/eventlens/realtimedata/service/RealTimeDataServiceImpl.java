package com.openshiftapps.eventlens.realtimedata.service;

import com.openshiftapps.eventlens.exception.ItemNotFoundException;
import com.openshiftapps.eventlens.realtimedata.dao.RealTimeDataDao;
import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RealTimeDataServiceImpl implements RealTimeDataService {

    private final RealTimeDataDao realTimeDataDao;

    @Override
    @Transactional
    public RealTimeData fetchRealTimeDataByVehicleId(Integer id) throws ItemNotFoundException {
        try {
            return realTimeDataDao.fetchRealTimeDataByVehicleId(id);
        } catch (Exception e) {
            log.warn(e.getMessage());
            throw new ItemNotFoundException();
        }
    }

    @Override
    @Transactional
    public void insertRealTimeData(Integer vehicleId, RealTimeData realTimeData) throws ItemNotFoundException {
        try {
            Integer result = realTimeDataDao.insertRealTimeData(vehicleId, realTimeData);

            if (!result.equals(1)) {
                throw new ItemNotFoundException();
            }
        } catch (DataIntegrityViolationException e) {
            throw new ItemNotFoundException();
        }
    }
}
