package com.openshiftapps.eventlens.realtimedata.service;

import com.openshiftapps.eventlens.exception.ItemNotFoundException;
import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;

public interface RealTimeDataService {
    RealTimeData fetchRealTimeDataByVehicleId(Integer id) throws ItemNotFoundException;

    void insertRealTimeData(Integer vehicleId, RealTimeData realTimeData) throws ItemNotFoundException;
}
