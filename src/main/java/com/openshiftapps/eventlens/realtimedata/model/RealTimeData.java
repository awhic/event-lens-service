package com.openshiftapps.eventlens.realtimedata.model;

import lombok.Builder;
import lombok.Data;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
public class RealTimeData {
    private Integer batteryStatus;
    private Integer currentMileage;
    private String motorHealth;
    private List<String> errorCodes;
    private Integer brakePadCondition;
    private Integer brakeFluidLevel;
    private Boolean tirePressureLow;
    private Boolean airConditioningFunctional;
    private Boolean softwareUpToDate;
    private String groupUuid;
    private Timestamp timestamp;
}

