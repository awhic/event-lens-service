package com.openshiftapps.eventlens.realtimedata.dao;

import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;

import java.util.List;

public interface RealTimeDataDao {
    RealTimeData fetchRealTimeDataByVehicleId(Integer id);

    List<RealTimeData> fetchRealTimeDataByGroupUuid(Integer id, String uuid);

    Integer insertRealTimeData(Integer id, RealTimeData realTimeData);
}
