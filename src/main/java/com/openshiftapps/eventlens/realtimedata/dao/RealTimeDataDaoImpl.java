package com.openshiftapps.eventlens.realtimedata.dao;

import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the {@link RealTimeDataDao} interface providing data access operations for vehicle real-time data.
 *
 * <p>Methods include:
 * <ul>
 *   <li><b>fetchRealTimeDataByVehicleId:</b> Retrieves real-time data for a specific vehicle identified by its ID.</li>
 *   <li><b>fetchRealTimeDataByGroupUuid:</b> Fetches a list of real-time data for trucks that are part of a group identified by a UUID.</li>
 *   <li><b>insertRealTimeData:</b> Inserts real-time data for a given vehicle identified by its vehicle ID.</li>
 * </ul>
 *
 * <p>The JDBC calls are initialized upon bean construction for efficiency.</p>
 *
 * <p><b>Note:</b> Data Layer was designed in an early conception where the application worked for long-haul trucks,
 * hence the mentions of "truck". The application is designed for all types of electric vehicle fleets.<p>
 *
 * @see RealTimeData
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class RealTimeDataDaoImpl implements RealTimeDataDao {

    private final JdbcTemplate realTimeDataJdbcTemplate;

    private SimpleJdbcCall fetchRealTimeDataByVehicleIdCall;
    private SimpleJdbcCall fetchRealTimeDataByGroupUuid;
    private SimpleJdbcCall insertRealTimeDataCall;

    @PostConstruct
    void initialize() {
        fetchRealTimeDataByVehicleIdCall = new SimpleJdbcCall(realTimeDataJdbcTemplate)
                .withFunctionName("fetch_real_time_data_by_truck_id")
                .declareParameters(
                        new SqlParameter("pi_truck_id", Types.INTEGER),
                        new SqlOutParameter("ref_cursor", Types.REF_CURSOR, new RealTimeDataRowMapper())
                )
                .withoutProcedureColumnMetaDataAccess();

        log.info("Initialized {}", fetchRealTimeDataByVehicleIdCall.getProcedureName());

        fetchRealTimeDataByGroupUuid = new SimpleJdbcCall(realTimeDataJdbcTemplate)
                .withFunctionName("fetch_real_time_data_by_group_uuid")
                .declareParameters(
                        new SqlParameter("pi_vehicle_id", Types.INTEGER),
                        new SqlParameter("pi_group_uuid", Types.VARCHAR),
                        new SqlOutParameter("ref_cursor", Types.REF_CURSOR, new RealTimeDataRowMapper())
                )
                .withoutProcedureColumnMetaDataAccess();

        log.info("Initialized {}", fetchRealTimeDataByVehicleIdCall.getProcedureName());

        insertRealTimeDataCall = new SimpleJdbcCall(realTimeDataJdbcTemplate)
                .withFunctionName("insert_truck_real_time_data")
                .declareParameters(
                        new SqlParameter("pi_truck_id", Types.INTEGER),
                        new SqlParameter("pi_battery_status", Types.INTEGER),
                        new SqlParameter("pi_current_mileage", Types.INTEGER),
                        new SqlParameter("pi_motor_health", Types.VARCHAR),
                        new SqlParameter("pi_error_codes", Types.VARCHAR),
                        new SqlParameter("pi_brake_pad_condition", Types.INTEGER),
                        new SqlParameter("pi_brake_fluid_level", Types.INTEGER),
                        new SqlParameter("pi_tire_pressure_low", Types.BOOLEAN),
                        new SqlParameter("pi_air_conditioning_functional", Types.BOOLEAN),
                        new SqlParameter("pi_software_up_to_date", Types.BOOLEAN),
                        new SqlParameter("pi_timestamp", Types.TIMESTAMP),
                        new SqlParameter("pi_group_uuid", Types.VARCHAR),
                        new SqlOutParameter("rows_inserted", Types.INTEGER)
                )
                .withoutProcedureColumnMetaDataAccess();

        log.info("Initialized {}", insertRealTimeDataCall.getProcedureName());
    }

    @Override
    @SuppressWarnings("unchecked")
    public RealTimeData fetchRealTimeDataByVehicleId(Integer id) {
        SqlParameterSource in = new MapSqlParameterSource().addValue("pi_truck_id", id);
        Map<String, Object> out = fetchRealTimeDataByVehicleIdCall.execute(in);

        log.debug("fetchRealTimeDataByTruckId() parameters: {}", in);
        log.trace("RefCursor: {}", out.get("ref_cursor"));

        return ((List<RealTimeData>) out.get("ref_cursor")).get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<RealTimeData> fetchRealTimeDataByGroupUuid(Integer id, String uuid) {
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pi_vehicle_id", id)
                .addValue("pi_group_uuid", uuid);
        Map<String, Object> out = fetchRealTimeDataByGroupUuid.execute(in);

        log.debug("fetchRealTimeDataByGroupUuid() parameters: {}", in);
        log.trace("RefCursor: {}", out.get("ref_cursor"));
        return ((List<RealTimeData>) out.get("ref_cursor"));
    }

    @Override
    public Integer insertRealTimeData(Integer id, RealTimeData realTimeData) {
        String errorCodesString = formatErrorCodes(realTimeData.getErrorCodes());

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("pi_truck_id", id)
                .addValue("pi_battery_status", realTimeData.getBatteryStatus())
                .addValue("pi_current_mileage", realTimeData.getCurrentMileage())
                .addValue("pi_motor_health", realTimeData.getMotorHealth())
                .addValue("pi_motor_health", realTimeData.getMotorHealth())
                .addValue("pi_error_codes", errorCodesString)
                .addValue("pi_brake_pad_condition", realTimeData.getBrakePadCondition())
                .addValue("pi_brake_fluid_level", realTimeData.getBrakeFluidLevel())
                .addValue("pi_tire_pressure_low", realTimeData.getTirePressureLow())
                .addValue("pi_air_conditioning_functional", realTimeData.getAirConditioningFunctional())
                .addValue("pi_software_up_to_date", realTimeData.getSoftwareUpToDate())
                .addValue("pi_timestamp", Timestamp.from(Instant.now().atZone(ZoneId.of("UTC")).toInstant()))
                .addValue("pi_group_uuid", realTimeData.getGroupUuid());
        Map<String, Object> out = insertRealTimeDataCall.execute(in);

        log.debug("insertRealTimeData() parameters: {}", in);
        return (Integer) out.get("rows_inserted");
    }

    private String formatErrorCodes(List<String> errorCodes) {
        if (errorCodes == null || errorCodes.isEmpty()) {
            return "";
        }
        return String.join(",", errorCodes);
    }
}
