package com.openshiftapps.eventlens.realtimedata.dao;

import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RealTimeDataRowMapper implements RowMapper<RealTimeData> {

    @Override
    public RealTimeData mapRow(ResultSet rs, int rowNum) throws SQLException {

        return RealTimeData.builder()
                .batteryStatus(rs.getInt("battery_status"))
                .currentMileage(rs.getInt("current_mileage"))
                .motorHealth(rs.getString("motor_health"))
                .errorCodes(parseErrorCodes(rs.getString("error_codes")))
                .brakePadCondition(rs.getInt("brake_pad_condition"))
                .brakeFluidLevel(rs.getInt("brake_fluid_level"))
                .tirePressureLow(rs.getBoolean("tire_pressure_low"))
                .airConditioningFunctional(rs.getBoolean("air_conditioning_functional"))
                .softwareUpToDate(rs.getBoolean("software_up_to_date"))
                .timestamp(rs.getTimestamp("timestamp"))
                .groupUuid(rs.getString("group_uuid"))
                .build();
    }

    private List<String> parseErrorCodes(String errorCodesString) {
        if (errorCodesString != null && !errorCodesString.trim().isEmpty()) {
            return Arrays.asList(errorCodesString.split(","));
        }
        return new ArrayList<>();
    }
}
