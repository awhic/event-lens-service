package com.openshiftapps.eventlens.realtimedata.controller;

import com.openshiftapps.eventlens.exception.ItemNotFoundException;
import com.openshiftapps.eventlens.realtimedata.model.RealTimeData;
import com.openshiftapps.eventlens.realtimedata.service.RealTimeDataService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@EnableKafka
@Slf4j
@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping("api/v1/real-time")
public class RealTimeDataController {

    private final RealTimeDataService realTimeDataService;

    @GetMapping("vehicle")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Fetch the latest real-time data for a vehicle")
    public RealTimeData fetchRealTimeData(@RequestParam Integer id) throws ItemNotFoundException {
        return realTimeDataService.fetchRealTimeDataByVehicleId(id);
    }
}
