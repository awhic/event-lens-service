package com.openshiftapps.eventlens.exception;

public class NotEnoughDataException extends Exception {
    public NotEnoughDataException() {
        super();
    }
}
