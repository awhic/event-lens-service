package com.openshiftapps.eventlens.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionAdvice {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ItemNotFoundException.class)
    public void handleItemNotFound() {}

    @ResponseStatus(HttpStatus.TOO_EARLY)
    @ExceptionHandler(NotEnoughDataException.class)
    public void notEnoughData() {}
}
